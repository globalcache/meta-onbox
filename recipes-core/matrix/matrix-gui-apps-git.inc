LICENSE = "CC-BY-SA"
#By default all Matrix v2 applications use the same license.
LICENSE_CHECKSUM = "LICENSE;md5=6e0ae7214f6c74c149cb25f373057fa9"
LIC_FILES_CHKSUM := "file://../${LICENSE_CHECKSUM}"

SRC_URI = "http://bitbucket.org/2bitmicro/matrix-gui-v2-apps.git"
SRCREV = "7bc9afc5fcee52a53c1ddf3becfadcf9f17c30d1"

SRC_URI[md5sum] = "bcee7cb79eca9aad0f2f1340bae12cff"
SRC_URI[sha256sum] = "64d46c370ff0ebea12daefda08c34aebcad61a63b12b9fa1ddc28015ed309cdb"

INC_PR = "pr5"

# Pull in the base package for installing matrix applications
require matrix-gui-apps.inc
