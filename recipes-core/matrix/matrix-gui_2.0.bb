DESCRIPTION = "Matrix GUI Application launcher"
HOMEPAGE = "https://gforge.ti.com/gf/project/matrix-gui-v2/"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=a886c9ef769b2d8271115d2502512e5d"

SECTION = "multimedia"

PR = "r3"

INITSCRIPT_NAME = "matrix-gui-2.0"
INITSCRIPT_PARAMS = "defaults 97"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit update-rc.d

SRC_URI[md5sum] = "c8a247f37769e6e9c0717b6cbd073745"
SRC_URI[sha256sum] = "6600c44031f2b04f70740f584b21f79c2eae6c42b59f1efac4a947f93a57eda1"
SRC_URI[md5sum] = "13b6b662ccb4e778ca06a0c6207987b1"
SRC_URI[sha256sum] = "4d04532fc507dbec8dbaf83dcebc319f7e114cc260ef1550150a4aaeab94335e"

BRANCH ?= "master"
SRCREV = "0b7623d29ccc6e72aee7bf4cc2a9527b19a2ea23"

SRC_URI = "git://bitbucket.org/2bitmicro/matrix-gui-v2.git;protocol=http \
           file://init \
           file://php.ini"

require matrix-gui-paths.inc

S = "${WORKDIR}/git"

MATRIX_ROT = ""

do_install(){
	install -d ${D}${MATRIX_BASE_DIR}
	install -d ${D}${MATRIX_WEB_DIR}
	cp -rf ${S}/* ${D}${MATRIX_WEB_DIR}

	# Install our php.ini file
	install -m 0644 ${WORKDIR}/php.ini ${D}${MATRIX_BASE_DIR}/

	# Set the proper path in the init script
	sed -i -e s=__MATRIX_WEB_DIR__=${MATRIX_WEB_DIR}= ${WORKDIR}/init
	sed -i -e "s/__MATRIX_ROT__/\"${MATRIX_ROT}\"/" ${WORKDIR}/init

	# Install the init script
	# TODO: replace init script with systemd files
	install -d ${D}${sysconfdir}/init.d
	install -m 0755 ${WORKDIR}/init ${D}${sysconfdir}/init.d/matrix-gui-2.0
}

GUIDEPS = "matrix-gui-browser refresh-screen"
GUIDEPS_keystone = ""

RDEPENDS_${PN} += "matrix-lighttpd-config lighttpd lighttpd-module-cgi lighttpd-module-compress lighttpd-module-expire php php-cgi php-cli ${GUIDEPS}"

FILES_${PN} += "${MATRIX_BASE_DIR}/*"
FILES_${PN} += "/usr/share/matrix-gui-2.0/*/*/*"
FILES_${PN} += "/etc/init.d/*"
FILES_${PN} += "/usr/share/matrix-gui-2.0/*"
FILES_${PN} += "/usr/share/matrix-gui-2.0/images/*"
FILES_${PN} += "/usr/share/matrix-gui-2.0/css/*"
