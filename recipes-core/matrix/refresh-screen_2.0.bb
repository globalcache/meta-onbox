DESCRIPTION = "Simple Application to force a screen refresh"
HOMEPAGE = "https://bitbucket.org/2bitmicro/refresh-screen"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://main.cpp;beginline=9;endline=37;md5=884b90f5bf0d711fe32c4f04b5276496"
SECTION = "multimedia"

SRC_URI[md5sum] = "461ed5800c4b0ae063d89f97791ff412"
SRC_URI[sha256sum] = "719733b1ea170178a9ade7ec841a86cb3d81b36835dcb61269310bf66e4231e3"
SRC_URI[md5sum] = "0b92e1408e6d1bacdbbad538672e3103"
SRC_URI[sha256sum] = "599fadcbfa964f9a83f62586dbb6911fa81445db367c4eb45d0d3a5442b6de96"
SRC_URI[md5sum] = "d2363c1b8767db318d1005b9954f1327"
SRC_URI[sha256sum] = "cb0dea97fd0b61d085258da5ca59c9436a550490c43bd6e0fdb5aeafec980664"

# Make sure that QT font libraries have been installed
RDEPENDS_${PN} += "qt4-embedded-fonts"

PR = "r0"

SRCREV = "23acf023743e864bb30158636840a080e36d45b6"

SRC_URI = "http://bitbucket.org/2bitmicro/refresh-screen.git;protocol=http"

S = "${WORKDIR}/git"

inherit qt4e

do_install() {
    install -d ${D}/${bindir}
    install -m 0755 ${S}/refresh_screen ${D}/${bindir}
}
